import React from 'react'
import './Section3.css'

export default function Section3() {
    return (
        <>
            <div className='container'>
                <div className='card1'>
                    <img className='img-tag' src='./Assets/dungeon.png' alt=''></img>
                    <h3>UAE Free Zone Company</h3>
                    <p className='section3-para'>Your registration agent, will answer all
                        of your questions and help you to reach
                        a conclusion that meets your objectives
                        of investing in the UAE.</p>
                    <a className='anchor-tag' href=' /'>Get Started</a>
                </div>
                <div className='card2'>
                    <img  className='img-tag' src='./Assets/Vector-2.png' alt=''></img>
                    <h3>Dubai Local Companies</h3>
                    <p className='section3-para'>The Dubai LLC formation documents
                        are actually articles of organization or a
                        certificate of organization. You can get
                        yours today.</p>
                    <a className='anchor-tag' href='/'>Get Started</a>
                </div>
                <div className='card3'>
                    <img  className='img-tag' src='./Assets/Vector-1.png' alt=''></img>
                    <h3>Offshore Compamies</h3>
                    <p className='section3-para'>You can register an offshore company
                        and open its bank account in Dubai.
                        Your agent will help you along all the
                        process</p>
                    <a className='anchor-tag' href='/'>Get Started</a>
                </div>


            </div>
        </>
    )
}
