import React from 'react'
import './Section4.css'
export default function Section4() {
    return (
        <>

            <div className='section4-container'>
                <div className='section4-container1'>
                    <div className='section4-container-item-1'>
                        <p>
                            Learn the ways in which you can benefit
                            from a UAE/Offshore Company. Then get
                            started on fulfilling your UAE dream.
                        </p>
                        <a href='/'>Claim a Free Quote</a>
                    </div>
                    <div className='section4-container-item-2'>
                        <img src='./Assets/ILLUSTRATION 03  1.png' alt='' />
                        <h2>Bank Account Setup</h2>
                        <p>There are many banks in the United
                            Arab Emirates [UAE]. Both locally
                            owned and branches of
                            multinational ones. Foreign banks
                            adjust according to their parent ’s
                            strategies and have changed local
                            requirements overnight in the past.
                            But we are here to help you.</p>
                        <div> <a href='/'>Learn More</a>
                            <img src='/Assets/Vector.png' alt='' /></div>
                    </div>
                    <div className='section4-container-item-3'>
                        <img src='./Assets/Group-2.png' alt='' />
                        <h2>UAE Company Visas</h2>
                        <p>Your application for visas is critical
                            especially if you intend to move to
                            Dubai. This becomes even more
                            urgent if your family will also move
                            with you. All the paperwork is done
                            on your behalf smoothly so that you
                            may only focus on doing what
                            matters most to you.</p>
                        <div> <a href='/'>Learn More</a>
                            <img src='/Assets/Vector.png' alt='' /></div>
                    </div>
                </div>
                <div className='section4-container2'>
                    <div className='section4-container2-item-1' >
                        <img src='./Assets/Group.png' alt='' />
                        <h2>Advice & Guidance</h2>
                        <p>All activities in the UAE are licensed.
                            Whether manufacturing, finance,
                            , trading marketing, consultancy or
                            restaurants. In some countries only
                            manufacturing is licensed. In others
                            there is a threshold below which
                            business are encouraged. Get our
                            insightfull guidance today.</p>
                        <div> <a href='/'>Learn More</a>
                            <img src='/Assets/Vector.png' alt='' /></div>
                    </div>
                    <div className='section4-container2-item-2'>
                        <img src='./Assets/Group-1.png' alt=''></img>
                        <h2>Registration Document
                            Perparation</h2>
                        <p>Several documents must be
                            prepared to start the process of
                            registering a new company in the
                            United Arab Emirates. Be it a Dubai
                            local company, a free zone one or an
                            offshore entity. Your registered
                            agent is dedicated to get this done
                            for you for a seamless process.</p>
                        <div> <a href='/'>Learn More</a>
                            <img src='/Assets/Vector.png' alt='' /></div>
                    </div>
                    <div className='section4-container2-item-3'>
                        <p>We have gathered a team of
                            professionals to craft adequate
                            services you can rely on for a friction
                            Learn more
                            free setup in UAE.</p>
                        <div> <a href='/'>More about our services</a>
                            <img src='/Assets/Vector.png' alt='' /></div></div>
                </div>
            </div>
            <div className='section4-container3'>
                <h2><span className='span-tag-2'>Most popular affordable pricing</span> per
                    jurisdictions are brought to you to
                    kick off your adventure.</h2>
            </div>

        </>
    )
}
