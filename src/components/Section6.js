import React from 'react'
import './Section6.css'

export default function Section6() {
    return (
        <>
            <div className='section6-container'>
                <div className='section6-container1'>
                    <div className='section6-container1-item-1'>
                        <p>Read about our blogs for more
                            information on our processes</p>
                    </div>
                    <div>
                        <div className='section6-conatianer-item-2'>
                            <img className='section6-conatianer-item-img' src='./Assets/Vector-4.png' alt='' />
                            <div>
                                <p className='section6-conatianer-item-para'>How to start a company
                                    formation in Dubai</p>
                                <span className='section6-conatianer-item-span'>5 Minutes</span>
                            </div>
                        </div>

                        <div className='section6-conatianer-item-2'>

                            <img className='section6-conatianer-item-img' src='./Assets/Vector-4.png' alt='' />
                            <div>
                                <p>How to start an Offshore
                                    company formation in Dubai</p>
                                <span className='section6-conatianer-item-span'>5 Minutes</span>
                            </div>
                        </div>

                        <div className='section6-conatianer-item-2'>
                            <img className='section6-conatianer-item-img' src='./Assets/Vector-4.png' alt='' />
                            <div>
                                <p>SEO Dubai: Who benefits
                                    the most?</p>
                                <span className='section6-conatianer-item-span'>5 Minutes</span>
                            </div>
                        </div>


                    </div>
                    <div className='section6-container-2'>

                        <p>Satified We are When Our<br></br>
                            Customers Are Happy</p>
                        <div className='section6-container-2-para-2'>
                            <div className='section6-container-2-para-2-items'>
                                <img className='section6-container-2-para-2-items-img' src='./Assets/Vector-3.png' alt='' />
                                <p className='section6-para-2'>
                                    "I am very happy with them. I’ll
                                    continue to use their services in
                                    future & highly recommend them
                                    to anyone
                                    , “
                                    <h4 className='header-container'>Muhib Abrar</h4>
                                </p>
                            </div>
                            <div className='section6-container-2-para-2-items'>
                                <img className='section6-container-2-para-2-items-img' src='./Assets/Vector-3.png' alt='' />
                                <p className='section6-para-2'>
                                    “
                                    Rama has a high level of integrity,
                                    intellect, knowledge of his
                                    business, resourcefulness and
                                    humanity, "
                                    <h4 className='header-container'>Colin Saldahna</h4>
                                </p>
                            </div>
                            <div className='section6-container-2-para-2-items'>
                                <img className='section6-container-2-para-2-items-img' src='./Assets/Vector-3.png' alt='' />
                                <p className='section6-para-2'>
                                    <b>"</b>  Your advise was so complete that I
                                    actually realized how beneficial
                                    this would be to my clients and my
                                    business. “
                                    <h4 className='header-container'>  Mark Swann</h4>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div className='section6-container2'>
                    <div className='section6-container2-para-1'>
                        <p>Get to know the whole us and
                            more of our services</p>
                    </div>
                    <div className='section6-container2-items2'>
                        <span id='section6-container2-span-1'>Services</span>
                        <h5>Products</h5>
                        <h5>  Solutions</h5>
                        <h5> Assurance</h5>
                        <h5>FAQ</h5>
                        <h5> Working at Varal-Singhania</h5>
                        <span id='section6-container2-span-1'>Policies</span>
                        <h5>Privacy Policy</h5>
                        <h5>Terms & Condition</h5>
                        <h5> About</h5>
                        <h5>About US</h5>
                        <h5>Contact US</h5>
                        <span id='section6-container2-span-1'>Address</span>
                        <p>608 One Lake Plaza, Cluster T,

                            Al Sarayat Street, Jumeirah
                            Lake Towers

                            Dubai

                            United Arab Emirates

                        </p>
                        <p>Office Hours: Sunday to
                            Thursday 8:30 AM to 6:30 PM
                            [GMT+4]

                            M: +971 55 794 2016

                            O: +971 4 447 2061</p>
                        <h5>Subscribe Now
                        </h5>
                        <p>Newsletters about amazing
                            opportunities in Dubai</p>



                        <div className="wrap-conatiner-2">
                            <div className="search-2">
                                <input type="text" className="searchTerm-2" placeholder="Enter email address" />
                                <button  className="searchButton-2">
                                    <img className='wrap-conatiner-img' src='./Assets/Group 34.png' alt=''></img>
                                </button>
                            </div>
                        </div>
                        <div className='icons-container'>
                            <img src='./Assets/Vector-5.png' alt='' />
                            <img src='./Assets/Vector-6.png' alt='' />
                            <img src='./Assets/Vector-8.png' alt='' />
                            <img src='.//Assets/Vector-7.png' alt='' />
                        </div>
                    </div>

                </div>
            </div>
            <div className='footer-conatiner'><p>Varaluae 2021 C
                All Right Reserved</p></div>



        </>
    )
}
