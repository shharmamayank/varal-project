import React from 'react'
import './Section5.css'

export default function Section5() {
    return (
        <>
            <div className='section5-container'>
                <div className='section5-container-1'>
                    <h3>AJMAN OFFSHORE</h3>
                    <h4>$2,997
                        <span className='span-tag-item'> One Time Payment</span></h4>
                    <h5>AJMAN OFFSHORE New Company formation includes</h5>
                    <li className='list-item-1'>Attestation Charges</li>
                    <li className='list-item-1'>Registered Agent</li>
                    <li className='list-item-1'>Registered Office</li>
                    <li className='list-item-1'>Varal Administrative</li>
                    <li className='list-item-1'>Preparing Statutory File</li>
                    <li className='list-item-1'> Liasing with Registration Authorities</li>
                    <li className='list-item-1'>Due Diligence Appraisal</li>
                    <li className='list-item-1'>Keeping The Register</li>
                    <li className='list-item-1'>Seal Charges</li>
                    <li className='list-item-1'>Certificate of Good Standing</li>
                    <button className='btn'>Continue</button>
                </div>
                <div className='section5-container-2'>
                    <h3>RAK ICC</h3>
                    <h4>$2,997
                        <span className='span-tag-item'> One Time Payment</span></h4>
                    <h5>Rak cc New Company formation includes</h5>
                    <li className='list-item-1'>Attestation Charges</li>
                    <li className='list-item-1'>Registered Agent</li>
                    <li className='list-item-1'>Registered Office</li>
                    <li className='list-item-1'>Varal Administrative</li>
                    <li className='list-item-1'>Preparing Statutory File</li>
                    <li className='list-item-1'>Liasing with Registration Authorities</li>
                    <li className='list-item-1'>Due Diligence Appraisal</li>
                    <li className='list-item-1'>Keeping The Register</li>
                    <li className='list-item-1'>Seal Charges</li>
                    <li className='list-item-1'>Certificate of Good Standing</li>
                    <button className='btn'>Continue</button>
                </div>
                <div className='section5-container-3'>
                    <h3>SHARJAH MEDIA CITY</h3>
                    <h4>$4,124
                        <span className='span-tag-item'> One Time Payment</span></h4>
                    <h5>Sharjah Media City New Company formation include</h5>
                    <li className='list-item-1' > Attestation Charges</li >
                    <li className='list-item-1'>Registered Agent</li >
                    <li className='list-item-1'>Registered Office</li >
                    <li className='list-item-1'>Varal Administrative</li >
                    <li className='list-item-1'>Preparing Statutory File</li>
                    <li className='list-item-1'>Liasing with Registration Authorities</li >
                    <li className='list-item-1'>Due Diligence Appraisal</li>
                    <li className='list-item-1'>Keeping The Register</li >
                    <li className='list-item-1'>Seal Charges</li>
                    <li className='list-item-1'>Certificate of Good Standing</li>
                    <button className='btn'> Continue</button>
                </div>

            </div>

        </>
    )
}
