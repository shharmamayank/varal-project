import React from 'react'
import "./SearchBar.css"

export default function SearchBar() {
  return (
    <>
      <div className="wrap">
        <div className="search">
          <button type="submit" class="searchButton">
          <i className="fa fa-search"></i>
          </button>
         
          <input type="text" className="searchTerm" placeholder="Search a term | Topic" />

        </div>
      </div>
      
      {/* <div class="box">

        <i class="fa fa-search" aria-hidden="true"></i>

        <input type="text" name=""/>

      </div> */}

    </>
  )
}
