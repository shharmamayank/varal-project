import React from 'react'
import "./Section1.css"

export default function Section1() {
    return (
        <>
            <div className='section1-flex'>
                <div className='section1-item-1'>
                    <a href='/'>Claim a Free Quote
                    </a>

                    <h2><b><span className='section1-span-1'>Get started </span>on fulfilling
                        your Dubai or UAE
                        dream.</b></h2>
                    <a href='/'>UAE & Offshore Company</a>
                    <p className='section1-para-1'>We provide you with information about UAE or
                        Offshore Company Registration & help you
                        setup your company with a bank account and
                        visas.</p>
                    <div>
                        <button className='section1-btn-1' >Start a Company</button>
                        <button className='section1-btn-2'> Renew a Company</button>
                    </div>

                </div>

                <div className='section1-item-2'>
                    <img src='./Assets/ILLUSTRATION.png' alt='' />
                </div>


            </div>
        </>
    )
}
