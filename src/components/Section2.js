import React from 'react'
import "./Section2.css"

export default function Section2() {
    return (
        <>
            <div className='Secton2-items'>
                <div> <a href='/'>More about our services</a>
                    <img src='./Assets/Group 117.png' alt='' /></div>
                <span className='span-tag-1'>Dedicated</span><h1 className='Section2-item1-h1'>to our mission we are</h1>
                <p className='Section2-item1-p'>Our services include Company Formation & Renewals,
                    Trust & Fiduciary, Tax Residency Setup With Family, Bank
                    Accounts, Remote Management, Stock Trading
                    Platforms, Ownership Solutions.</p>
            </div>
        </>
    )
}
