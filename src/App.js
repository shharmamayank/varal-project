import './App.css';
import Navbar from './components/Navbar';
import SearchBar from './components/SearchBar';
import Section1 from './components/Section1';
import Section2 from './components/Section2';
import Section3 from './components/Section3';
import Section4 from './components/Section4';
import Section5 from './components/Section5';
import Section6 from './components/Section6';
// import { CiSearch } from "react-icons/fa";
function App() {
  return (
    <>
      <body>
        <Navbar />
        <div>
          <SearchBar />
        </div>
        <div><Section1 /></div>
        <div><Section2 /></div>
        <div><Section3 /></div>
        <div><Section4 /></div>
        <div><Section5 /> </div>
        <div><Section6 /></div>
      </body>
    </>
  );
}

export default App;
